# tmux config 
john.tmux.conf -> ~/.tmux.conf  

## Dracula config
`dracula-theme.tmux.conf`  
[Dracula color theme](https://cassidy.codes/blog/2019-08-03-tmux-colour-theme/)  

## Add CPU/MEM stats 
[tmux-mem-cpu-load](https://github.com/thewtex/tmux-mem-cpu-load)  
Somthing else to look into  
[tmux](https://github.com/gpakosz/.tmux)  
